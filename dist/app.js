"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const passport_1 = __importDefault(require("passport"));
const passport_2 = __importDefault(require("./middlewares/passport"));
const rolePassport_1 = __importDefault(require("./middlewares/rolePassport"));
const auth_routes_1 = __importDefault(require("./routes/auth.routes"));
const user_routes_1 = __importDefault(require("./routes/user.routes"));
const path_1 = __importDefault(require("path"));
// Inicializaciones
const app = express_1.default();
// Configuraciones
app.set('port', process.env.PORT || 3000);
// Middlewares
app.use(morgan_1.default('dev'));
app.use(cors_1.default());
app.use(express_1.default.urlencoded({ extended: false }));
app.use(express_1.default.json());
app.use(passport_1.default.initialize());
passport_1.default.use(passport_2.default);
passport_1.default.use(rolePassport_1.default);
// Routes
app.get('/', (req, res, next) => {
    res.send(`La API esta en http://localhost:${app.get('port')}`);
});
app.use(auth_routes_1.default);
app.use('/users', user_routes_1.default);
// Folder public files
app.use('/uploads', express_1.default.static(path_1.default.resolve('uploads')));
exports.default = app;
