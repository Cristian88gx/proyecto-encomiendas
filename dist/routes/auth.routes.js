"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
const auth_controller_1 = require("../controller/auth.controller");
const router = express_1.Router();
const authenticateJwt = passport_1.default.authenticate("jwt", { session: false });
// Login
router.post("/signIn", auth_controller_1.signIn);
// Logout
router.post("/signOut", authenticateJwt, auth_controller_1.signOut);
// Info Profile
router.get("/profile", authenticateJwt, auth_controller_1.infoProfile);
exports.default = router;
