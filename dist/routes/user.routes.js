"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
const role_1 = __importDefault(require("../middlewares/role"));
const user_controller_1 = require("../controller/user.controller");
const multer_1 = __importDefault(require("../libs/multer"));
const router = express_1.Router();
const autenticateJwt = passport_1.default.authenticate("jwt", { session: false });
// Crear un usuario nuevo
router.post("/createUser", [multer_1.default.single("image"), autenticateJwt, role_1.default(["admin", "superadmin"])], user_controller_1.addNewUser);
// Obtener todos los usuarios
router.get("/getUsers", [autenticateJwt, role_1.default(["admin", "superadmin"])], user_controller_1.getUsers);
// Buscar un usuario por su id
router.get("/searchUser/:userId", [autenticateJwt, role_1.default(["admin", "superadmin"])], user_controller_1.getUserWithId);
// Modificar un usuario
router.put("/updateUser/:userId", [autenticateJwt, role_1.default(["admin", "superadmin"])], user_controller_1.updateUser);
// Eliminar un usuario por su id
router.delete("/deleteUser/:userId", [autenticateJwt, role_1.default(["admin", "superadmin"])], user_controller_1.deleteUser);
exports.default = router;
