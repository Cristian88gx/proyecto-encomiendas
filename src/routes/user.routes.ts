import { Router } from "express";
import passport from "passport";
import checkRole from "../middlewares/role";
import {
  addNewUser,
  getUserWithId,
  getUsers,
  updateUser,
  deleteUser,
} from "../controller/user.controller";
import multer from "../libs/multer";
const router = Router();
const autenticateJwt = passport.authenticate("jwt", { session: false });

// Crear un usuario nuevo
router.post(
  "/createUser",
  [multer.single("image"),autenticateJwt, checkRole(["admin", "superadmin"])],
  addNewUser
);
// Obtener todos los usuarios
router.get(
  "/getUsers",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  getUsers
);
// Buscar un usuario por su id
router.get(
  "/searchUser/:userId",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  getUserWithId
);
// Modificar un usuario
router.put(
  "/updateUser/:userId",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  updateUser
);
// Eliminar un usuario por su id
router.delete(
  "/deleteUser/:userId",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  deleteUser
);

export default router;
