import { Router } from "express";
import passport from "passport";
import { signIn, signOut, infoProfile } from "../controller/auth.controller";

const router = Router();
const authenticateJwt = passport.authenticate("jwt", { session: false });

// Login
router.post("/signIn", signIn);
// Logout
router.post("/signOut", authenticateJwt, signOut);
// Info Profile
router.get("/profile", authenticateJwt, infoProfile);

export default router;