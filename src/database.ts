import mongoose, {ConnectionOptions} from 'mongoose'
import config from './config/config'

const dbOptions: ConnectionOptions = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    user: config.DB.USER,
    pass: config.DB.PASSWORD,
};

mongoose.connect(config.DB.URI,dbOptions);

const connection = mongoose.connection
connection.once('open',() => {
    console.log('Mongodb Conectado')
})

connection.on('error',err => {
    console.log(err);
    process.exit(0)
})